//soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

//jawaban soal 1 urutkan dengan loop
for(var i = 0; i < daftarHewan.length; i++){
  for(var j = i+1; j < daftarHewan.length; j++){
    if(daftarHewan[i] > daftarHewan[j]){
      var urut = daftarHewan[i];
        daftarHewan[i] = daftarHewan[j];
        daftarHewan[j] = urut;
    }
  }
}

for(var i = 0; i <daftarHewan.length; i++){
    console.log(daftarHewan[i]);
}


//jawaban soal 2

function introduce(){
    return "Nama saya "+data.name+", umur saya "+data.age+" tahun, alamat saya di "+data.address+", dan saya punya hobby yaitu "+data.hobby;
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }

var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 



//jawaban soal 3

var hitung = 0;
var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")
hitung_2 = hitung_2 - hitung_1;
console.log(hitung_1 , hitung_2)

function hitung_huruf_vokal(huruf) {
    for(var i = 0; i < huruf.length; i++){
        if( huruf[i] == "a" || huruf[i] == "A" ||
            huruf[i] == "i" || huruf[i] == "I" ||
            huruf[i] == "u" || huruf[i] == "U" ||
            huruf[i] == "e" || huruf[i] == "E" ||
            huruf[i] == "o" || huruf[i] == "O"){
            hitung++;
        }
    }return hitung;
}


//jawaban soal 4

var hitung = function (angka){
  return (angka-1)*2;
}

console.log(hitung(0))
console.log(hitung(1))
console.log(hitung(2))
console.log(hitung(3))
console.log(hitung(5))

