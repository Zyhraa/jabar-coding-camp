//soal 1 keliling persegi panjang
const luas = (lebar, panjang) => {
    let hasil = panjang * lebar;
    console.log(hasil);
}
luas(10,4);

const keliling = (lebar, panjang) => {
    let hasil = (panjang + lebar) * 2;
    console.log(hasil);
}
keliling(1,10);



//soal 2
const newFunction = (firstName, lastName) => {
    return {fullName: function(){console.log(`${firstName} ${lastName}`)}
    }
}
//Driver Code 
newFunction("William", "Imoh").fullName()



//soal 3 destructuring
const newObject = {firstName: "Muhammad", lastName: "Iqbal Mubarok", address: "Jalan Ranamanyar",
hobby: "playing football"};
//jawaban
const {firstName, lastName, address, hobby} = newObject;
// Driver code
console.log(firstName, lastName, address, hobby)



//soal 4 kombinasi 2 array
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
//jawaban
const combined = [...west, ...east]
//Driver Code
console.log(combined);



//soal 5 sederhanakan string
const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 
//jawaban
const after = `lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}`