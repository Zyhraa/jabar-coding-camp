//soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

//jawaban soal 1. saya senang belajar JAVASCRIPT
var pertama1= pertama.substring(0,4);
var pertama2= pertama.substr(12,6);
var kedua1= kedua.substr(8,10).toUpperCase();
var kedua2= kedua.substring(0,7);

console.log(pertama1+" "+pertama2+" "+kedua2+" "+kedua1);


//=======================================//
//soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

//jawaban soal 2. hasil 24, 3 operasi
var jadint1= parseInt([kataPertama]);
var jadint2= parseInt([kataKedua]);
var jadint3= parseInt([kataKetiga]);
var jadint4= parseInt([kataKeempat]);

var hasil= (jadint4 - jadint3 + jadint1) * jadint2

console.log(hasil);

//=======================================//
//soal 3 dan jawaban
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua= kalimat.substr(4,10); 
var kataKetiga= kalimat.substr(15,3);
var kataKeempat= kalimat.substr(19,5);
var kataKelima= kalimat.substr(25,6); 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);