saya awal belajar coding pertama waktu saya masih di SMK TKJ, pertama 
kali dikenalkan dengan bahasa html dan membuat tampilan simpel yaitu 
hello world yang diberi warna background, garis pembatas, h1 h2 h3. sejak 
itu saya tertarik dengan programming. tetapi saya tidak tahu harus mulai 
dari mana untuk memulai belajar programming jadi waktu itu saya hanya 
berfokus pada jurusan saya, TKJ. sekarang saya sudah lulus dan sedang 
menjalani kuliah di jurusan Sistem informasi. disini saya sudah belajar 
beberapa bahasa seperti C++, Java, PHP, dan CSS. jujur saya paling 
tertarik dengan web programming maka dari itu saya mulai mendalami HTML, 
PHP, CSS. pas sekali saya diberi tugas besar dengan kelompok untuk UAS 
yaitu membuat website sesuai keinginan. kami diberi pilihan untuk memakai 
boostrap atau CSS. kami memilih untuk memakai CSS dan dari sini saya 
bersama teman saya meng explore CSS dan PHP. awalnya kami ingin 
menggunakan javascript juga tapi kami pikir waktunya tidak cukup untuk 
belajar bahasa baru dan waktu pun hanya tersisa sebulan. di akhir pun 
kami berdua begadang hampir seminggu untuk menyelesaikannya dan 
alhamdullilah selesai tepat waktu walaupun belum sepenuhnya jadi :D
