var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]

// Lanjutkan code untuk menjalankan function readBooksPromise
let reading = (time, books, timeout) => {
    if (timeout < books.length){
        readBooksPromise (time, books[timeout], function(sisa){
            if(sisa > 0){
                timeout+=1;
                reading(sisa, books, timeout)
            }
        })
    }
}

reading(10000, books, 0)
